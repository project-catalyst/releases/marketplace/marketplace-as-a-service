#!/usr/bin/env bash

projects=(information-broker access-manager market-clearing-manager market-session-manager)

function delete_project() {
    if [[ -d "$1" ]]; then
      echo -e "\tCleaning up source code for project $1."
      rm -rf $1
    else
      echo -e "\tSource code for project $1 does not exist, skipping.."
    fi
}

# Remove containers
cd docker/
sudo docker-compose down
cd -

# Cleanup source code
echo "Cleaning up projects source code"
for project in "${projects[@]}"; do
    delete_project ${project}
done

# Clean up configuration
echo "Cleaning up configuration"
if [[ -f "$(pwd)/docker/.env" ]]; then rm $(pwd)/docker/.env; fi
echo "Done."