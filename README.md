# CATALYST Marketplace as a Service
A service to allow for quick instantiation of a CATALYST Marketplace. Although this is part of the Marketplace-as-a-Service 
activities, the final version should be much richer

## Installation
```bash
$ bash deploy.sh
```

If everything went well, the output should be similar to:

```
CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS                      PORTS                    NAMES
7844d2473600        tomcat:8                              "catalina.sh run"        10 minutes ago      Up 10 minutes               0.0.0.0:8001->8080/tcp   tomcat
d5f67e23cb06        catalyst_marketplace_access-manager   "/bin/sh -c 'bash /o…"   10 minutes ago      Up 10 minutes               0.0.0.0:80->80/tcp       access-manager
44f7a2262254        catalyst_marketplace_ib               "/bin/sh -c ${INSTAL…"   10 minutes ago      Up 10 minutes               0.0.0.0:5000->5000/tcp   ib
7a9b3960ac16        catalyst_marketplace_msm              "/usr/local/bin/mvn-…"   10 minutes ago      Exited (0) 10 minutes ago                            msm
9c68d0e3269b        catalyst_marketplace_mcm              "/usr/local/bin/mvn-…"   10 minutes ago      Exited (0) 10 minutes ago                            mcm
3293d4ed4baf        postgres:10                           "docker-entrypoint.s…"   10 minutes ago      Up 10 minutes               5432/tcp                 db
```

The exited `msm` and `mcm` applications are normal as such: the purpose of these dockers is to build the war files and
provide them to the `tomcat` container.


If the source code gets updated on gitlab.com, this script should handle all the changes, just run the deployment script
once more; it should pull the new code, build the new image and deploy the updated services. The downtime should be 
really minimum (at the level of several seconds).

### Prerequisites
* docker and docker-compose
* The following ports should be opened at a firewall:
    * `80` for accessing the UI (Access Manager)
    * `5000` for accessing the API part (Information Broker)
   

## Configuration
The `docker/configuration.env` file is the main entry point for the configuration.
The following should change in all cases:

* All credentials
* The `INFORMATION_BROKER_URL` field (mostly the IP address)

## How to stop the service
To stop the service, perform the following commands:
```bash
$ cd docker/
$ sudo docker-compose down
```

If you deleted the source code or the generated `.env` file, issue the following commands:

```bash
$ sudo docker rm -f msm
$ sudo docker rm -f mcm
$ sudo docker rm -f tomcat
$ sudo docker rm -f access-manager
$ sudo docker rm -f ib
```

