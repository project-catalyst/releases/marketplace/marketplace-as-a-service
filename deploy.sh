#!/usr/bin/env bash

#########################################
# Get the source code of all components #
#########################################

# Information Broker
git_project="information-broker"
if [ -d "${git_project}" ]; then
  echo "Source code for ${git_project} already exists. Pulling.."
  cwd=$(pwd)
  cd ${git_project}
  rm config/docker/.env
  git pull
  cd ${cwd}
else
  echo "Source code for ${git_project} does not exist. Cloning.."
  git clone -b '1.0' --single-branch --depth 1 https://gitlab.com/project-catalyst/releases/marketplace/${git_project}.git
fi


# Access Manager
git_project="access-manager"
if [ -d "${git_project}" ]; then
  echo "Source code for ${git_project} already exists. Pulling.."
  cwd=$(pwd)
  cd ${git_project}
  rm .env
  git pull
  cd ${cwd}
else
  echo "Source code for ${git_project} does not exist. Cloning.."
  git clone -b '1.0' --single-branch --depth 1 https://gitlab.com/project-catalyst/releases/marketplace/${git_project}.git
fi

# Market Clearing Manager
git_project="market-clearing-manager"
if [ -d "${git_project}" ]; then
  echo "Source code for ${git_project} already exists. Pulling.."
  cwd=$(pwd)
  cd ${git_project}
  git pull
  cd ${cwd}
else
  echo "Source code for ${git_project} does not exist. Cloning.."
  git clone -b '1.0' --single-branch --depth 1 https://gitlab.com/project-catalyst/releases/marketplace/${git_project}.git
fi

# Market Session Manager
git_project="market-session-manager"
if [ -d "${git_project}" ]; then
  echo "Source code for ${git_project} already exists. Pulling.."
  cwd=$(pwd)
  cd ${git_project}
  git pull
  cd ${cwd}
else
  echo "Source code for ${git_project} does not exist. Cloning.."
  git clone -b '1.0' --single-branch --depth 1 https://gitlab.com/project-catalyst/releases/marketplace/${git_project}.git
fi


#########################################
#           Copy configuration          #
#########################################

echo "Copying configurations"
cp docker/configuration.env docker/.env
cp docker/.env access-manager
cp docker/.env information-broker/config/docker

# IFF running on linux, we could use sed commands to re-build the .env in a more configurable manner
# i.e. by using global configuration.
# Currently, I leave the configuration more verbose, to run on MAC and Linux,
# since sed does not run on Windows and runs differently on MAC than on Linux.
# When this is finalized, I will re-create the environment to make it less error-prone.
# Make sure to read the comments in the configuration.env file

#########################################
#      Deploy the mplace stack          #
#########################################

# If sudo is not available (e.g. Windows or su-based distros), replace accordingly.
cwd=$(pwd)
cd docker/
sudo docker-compose build && \
sudo docker-compose down && \
sudo docker-compose rm -f && \
sudo docker-compose up -d
cd ${cwd}
echo "Done."
